var lestado = "apagada"
var licuadora = document.getElementById("licuadora");
var boton = document.getElementById("l-b");
var sonido = document.getElementById("licuadora-sonido");

function controlaLicuadora() {
    if (lestado == "apagada"){
        lestado = "encendido";
        musica();
        licuadora.classList.add("activo");
        boton.classList.add("activo");
        
    }else {
        lestado = "apagada";
        musica()
        licuadora.classList.remove("activo")
        boton.classList.remove("activo")
    }
}

function musica(){
    if (sonido.paused){
        sonido.play();
    }else{
        sonido.pause();
        sonido.currentTime = 0;
    }
}